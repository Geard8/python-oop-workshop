from functools import wraps

# my decorator for 1. Write a custom decorator A part of Python: Practical OOP workshop mini assiment
def my_decorator_logger(logger_file_name = "my_test_logger.txt"):
    '''
    My_decorator_logger is a decorator what will write in a specifc logger_file_name that
    logs before and after that the function is running. 

    param: 
        logger_file_name: str name of logger file that will be writen to.
    return: What called funtion returns 
    '''
    def decorator(func):
        @wraps(func)
        def wrapper_func(*args, **kwargs):
            f = open(logger_file_name, "a")
            f.write(f"Running function: {func.__name__}\n")

            return_object = func(*args, **kwargs)

            f.write(f"Done running function: {func.__name__}\n\n")
            f.close()

            return return_object
        return wrapper_func
    return decorator

# fucntion to test docorator with parameter
@my_decorator_logger("my_test_logger_2.txt")
def my_test_function(my_text = "random text"):
    '''My function docstring'''
    print(f"HEREE IS MY TEST FUNCTION. and my_text: {my_text}")
    return my_text

if __name__ =="__main__":

    # call function to test docorator on this function
    my_return_value = my_test_function("NEW random text")
    print(my_return_value)

    # checking out my_test_function name and docstring to verify if it keep there own or if it is overwriten by decorator.
    print(my_test_function.__name__)
    print(my_test_function.__doc__)
